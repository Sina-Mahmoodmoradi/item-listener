import { Box } from '@mui/material'
import Item from './Item'
const Items = ({ items, removeItem }) => {
    return (
        <Box>
            {items.map((item) => (
                <Item key={item.key} removeItem={removeItem} item={item} />
            ))}
        </Box>
    )
}

export default Items
