import { Card, CardActions, CardContent } from '@mui/material'
// import { makeStyles } from '@mui/styles'
import AddItem from './AddItem'
import Options from './Options'
import Items from './Items'
import { useState } from 'react'

const Main = () => {
    const [items, setItems] = useState([
        {
            key: 1,
            title: 'this is first item',
        },
        {
            key: 2,
            title: 'this is another item',
        },
    ])

    const removeItem = (key) => {
        setItems(items.filter((item) => item.key != key))
    }

    const addItem = (text) => {
        let key = items.length + 1
        setItems([
            ...items,
            {
                key: key,
                title: text,
            },
        ])
    }

    return (
        <Card>
            <CardActions>
                <Options />
            </CardActions>
            <CardActions>
                <AddItem addItem={addItem} />
            </CardActions>
            <CardContent>
                {items.length > 0 ? (
                    <Items items={items} removeItem={removeItem} />
                ) : (
                    'There are no items'
                )}
            </CardContent>
        </Card>
    )
}

export default Main
