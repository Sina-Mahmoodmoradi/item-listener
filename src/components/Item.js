import { Box, Button } from '@mui/material'
import { makeStyles } from '@mui/styles'

const useStyle = makeStyles({
    item: {
        backgroundColor: 'lightblue',
        margin: '5px 0px',
    },
    button: {
        float: 'Right',
    },
    text: {
        padding: '9px',
    },
})

const Item = ({ item, removeItem }) => {
    const classes = useStyle()
    return (
        <Box className={classes.item}>
            <Button
                className={classes.button}
                onClick={() => removeItem(item.key)}
            >
                x
            </Button>
            <Box className={classes.text}>{item.title}</Box>
        </Box>
    )
}

export default Item
