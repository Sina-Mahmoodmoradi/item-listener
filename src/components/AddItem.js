import { Box, Button, TextField } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { useState } from 'react'
const useStyle = makeStyles({
    box: {
        display: 'flex',
        margin: '20px 0px',
        width: '100%',
    },
    textField: {
        flexBasis: '60%',
    },
    button: {
        flexBasis: '40%',
        borderRadius: '0',
    },
})
const AddItem = ({ addItem }) => {
    const [item, setItem] = useState('')

    const onSubmit = () => {
        if (!item) {
            alert('Input is empty')
            return
        }
        addItem(item)
        setItem('')
    }

    const classes = useStyle()
    return (
        <Box className={classes.box}>
            <TextField
                className={classes.textField}
                variant="standard"
                label="Item"
                value={item}
                onChange={(e) => setItem(e.target.value)}
            />
            <Button
                className={classes.button}
                variant="contained"
                onClick={onSubmit}
            >
                ADD Item
            </Button>
        </Box>
    )
}

export default AddItem
