import { Grid, MenuItem, TextField } from '@mui/material'
import { useState } from 'react'
const colors = [{ color: 'orange' }, { color: 'red' }, { color: 'blue' }]
const themes = [{ theme: 'white' }, { theme: 'dark' }]

const Options = () => {
    const [color, setColor] = useState('blue')
    const changeColorFieldValue = (e) => {
        setColor(e.target.value)
    }
    const [theme, setTheme] = useState('white')
    const changeThemeFieldValue = (e) => {
        setTheme(e.target.value)
    }
    return (
        <Grid container>
            <Grid item xs={6}>
                <TextField
                    variant="standard"
                    label="Color"
                    value={color}
                    onChange={changeColorFieldValue}
                    select
                    sx={{ width: '80%' }}
                >
                    {colors.map((item) => {
                        return (
                            <MenuItem key={item.color} value={item.color}>
                                {item.color}
                            </MenuItem>
                        )
                    })}
                </TextField>
            </Grid>
            <Grid item xs={6}>
                <TextField
                    variant="standard"
                    label="Theme"
                    value={theme}
                    onChange={changeThemeFieldValue}
                    select
                    sx={{ width: '80%' }}
                >
                    {themes.map((item) => {
                        return (
                            <MenuItem key={item.theme} value={item.theme}>
                                {item.theme}
                            </MenuItem>
                        )
                    })}
                </TextField>
            </Grid>
        </Grid>
    )
}

export default Options
